package be.kdg.mentoring.week04.f1.model;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Period;

public class Driver extends F1Entity {
    private String driverId;
    private int permanentNumber;
    private String code;
    private String url;
    private String givenName;
    private String familyName;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate dateOfBirth;
    private String nationality;

    public String getDriverId() {
        return driverId;
    }

    public void setDriverId(String driverId) {
        super.setId(driverId);
        this.driverId = driverId;
    }

    public int getPermanentNumber() {
        return permanentNumber;
    }

    public void setPermanentNumber(int permanentNumber) {
        this.permanentNumber = permanentNumber;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getGivenName() {
        return givenName;
    }

    public void setGivenName(String givenName) {
        this.givenName = givenName;
    }

    public String getFamilyName() {
        return familyName;
    }

    public void setFamilyName(String familyName) {
        this.familyName = familyName;
    }

    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(LocalDate dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    private static final SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");

    @Override
    public String toString() {
//        Period.between(birthDate, currentDate).getYears()
        return String.format("%s %s (%s) born: %s (%d) (%s)", givenName, familyName, driverId, dateOfBirth != null ? dateOfBirth : "N/A",
                dateOfBirth != null ? Period.between(dateOfBirth, LocalDate.now()).getYears() : 0, nationality);
    }
}
