package be.kdg.mentoring.week04.f1.repository.memory;

import be.kdg.mentoring.week04.f1.model.Driver;
import be.kdg.mentoring.week04.f1.repository.DriverRepository;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class MemoryDriverRepository extends MemoryF1Repository<Driver> implements DriverRepository {

    public MemoryDriverRepository(List<Driver> data) {
        super(data);
    }

    @Override
    public Collection<Driver> getDriverByCode(String code) {
        return data.stream().filter(d -> code.equals(d.getCode())).collect(Collectors.toList());
    }

    @Override
    public Collection<Driver> getDriverByLastName(String name) {
        return data.stream().filter(d -> d.getFamilyName().equalsIgnoreCase(name)).collect(Collectors.toList());
    }

    @Override
    public Collection<Driver> getDriverByGivenName(String name) {
        return data.stream().filter(d -> d.getGivenName().equalsIgnoreCase(name)).collect(Collectors.toList());
    }
}
