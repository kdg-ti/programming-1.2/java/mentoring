package be.kdg.mentoring.week04.f1.repository.memory;

import be.kdg.mentoring.week04.f1.model.F1Entity;
import be.kdg.mentoring.week04.f1.repository.F1Repository;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class MemoryF1Repository<T extends F1Entity> implements F1Repository<T> {

    protected List<T> data;

    public MemoryF1Repository(List<T> data) {
        this.data = data;
    }

    public MemoryF1Repository() {
    }

    @Override
    public Collection<T> getAll() {
        return Collections.unmodifiableCollection(data);
    }

    @Override
    public Collection<T> findById(String id) {
        return data.stream().filter(d -> id.equals(d.getId())).collect(Collectors.toList());
    }

    @Override
    public Collection<T> find(Predicate<T> filter) {
        return data.stream().filter(filter).collect(Collectors.toList());
    }
}
