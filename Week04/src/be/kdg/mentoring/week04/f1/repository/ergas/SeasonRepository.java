package be.kdg.mentoring.week04.f1.repository.ergas;

import be.kdg.mentoring.week04.f1.model.Season;
import com.fasterxml.jackson.core.type.TypeReference;

public class SeasonRepository extends ErgasF1Repository<Season> {
    private static final String ERGAS_API_URL = "https://ergast.com/api/f1/seasons.json?limit=1000";

    public SeasonRepository() {
        this(ERGAS_API_URL);
    }

    public SeasonRepository(String apiUrl) {
        super(apiUrl, new TypeReference<>() {
        });
    }
}
