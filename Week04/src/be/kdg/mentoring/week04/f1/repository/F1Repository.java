package be.kdg.mentoring.week04.f1.repository;

import be.kdg.mentoring.week04.f1.model.F1Entity;

import java.util.Collection;
import java.util.function.Predicate;

public interface F1Repository <T extends F1Entity> {

    Collection<T> getAll();

    Collection<T> findById(String id);

    Collection<T> find(Predicate<T> filter);
}
