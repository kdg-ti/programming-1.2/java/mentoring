package be.kdg.mentoring.week04.f1.repository;

import be.kdg.mentoring.week04.f1.model.*;
import be.kdg.mentoring.week04.f1.repository.ergas.ErgasF1Repository;
import be.kdg.mentoring.week04.f1.repository.memory.MemoryDriverRepository;
import com.fasterxml.jackson.core.type.TypeReference;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RepositoryFactory {

    private static final Map<String, F1Repository<? extends F1Entity>> registry = new HashMap<>();

    private static final String DRIVERS = "drivers";

    private static final String CONSTRUCTORS = "constructors";

    private static final String SEASONS = "seasons";

    private static final String CIRCUITS = "circuits";

    private static final String API_URL_DRIVERS = "https://ergast.com/api/f1/drivers.json?limit=1000";

    private static final String API_URL_CONSTRUCTORS = "https://ergast.com/api/f1/constructors.json?limit=1000";

    private static final String API_URL_SEASONS = "https://ergast.com/api/f1/seasons.json?limit=1000";

    private static final String API_URL_CIRCUITS = "https://ergast.com/api/f1/circuits.json?limit=1000";

    private static final String[][] champions = {
            {"Germany", "Michael Schumacher"},
            {"Argentina", "Juan Manuel Fangio"},
            {"France", "Alain Prost"},
            {"Germany", "Sebastian Vettel"},
            {"Australia", "Jack Brabham"},
            {"United Kingdom", "Jackie Stewart"},
            {"Austria", "Niki Lauda"},
            {"Brasil", "Nelson Piquet"},
            {"Brasil", "Ayrton Senna"},
            {"United Kingdom", "Lewis Hamilton"},
            {"Netherlands", "Max Verstappen"}
    };

    private static final Integer[][] years = {
            {1994, 1995, 2000, 2001, 2002, 2003, 2004},
            {1951, 1954, 1955, 1956, 1957},
            {1985, 1986, 1989, 1993},
            {2010, 2011, 2012, 2013},
            {1959, 1960, 1966},
            {1969, 1971, 1973},
            {1975, 1977, 1984},
            {1981, 1983, 1987},
            {1988, 1990, 1991},
            {2008, 2014, 2015, 2017, 2018, 2019, 2020},
            {2021, 2022, 2023}
    };

    static {
        mapRepo(new ErgasF1Repository<Driver>(
                API_URL_DRIVERS,
                new TypeReference<>() {
                }), DRIVERS);

        mapRepo(new ErgasF1Repository<Constructor>(
                API_URL_CONSTRUCTORS,
                new TypeReference<>() {
                }), CONSTRUCTORS);

        mapRepo(new ErgasF1Repository<Season>(
                API_URL_SEASONS,
                new TypeReference<>() {
                }), SEASONS);

        mapRepo(new ErgasF1Repository<Circuit>(
                API_URL_CIRCUITS,
                new TypeReference<>() {
                }), CIRCUITS);

        mapRepo(new MemoryDriverRepository(loadDriversFromSource()), "memory");
    }

    private static void mapRepo(F1Repository repo, String name) {
        registry.put(name, repo);
        registry.put(repo.getClass().getName(), repo);
        registry.put(repo.getClass().getSimpleName(), repo);
    }

    public static F1Repository<? extends F1Entity> create(String name) {
        return registry.get(name);
    }

    public static F1Repository<Driver> createDriverRepo() {
        return (F1Repository<Driver>) registry.get(DRIVERS);
    }

    public static F1Repository<Constructor> createConstructorRepo() {
        return (F1Repository<Constructor>) registry.get(CONSTRUCTORS);
    }

    public static F1Repository<Season> createSeasonRepo() {
        return (F1Repository<Season>) registry.get(CONSTRUCTORS);
    }

    public static F1Repository<Circuit> createCircuitRepo() {
        return (F1Repository<Circuit>) registry.get(CONSTRUCTORS);
    }

    public static void main(String[] args) {
        F1Repository<? extends F1Entity> constructors = create(CONSTRUCTORS);
        System.out.println(constructors.getAll());
    }

    private static List<Driver> loadDriversFromSource() {
        List<Driver> drivers = new ArrayList<>();
        for (int i = 0; i < champions.length; i++) {
            String[] championData = champions[i];
            Integer[] championYears = years[i];
            Driver driver = new Driver();
            driver.setFamilyName(championData[1].split(" ")[1]);
            driver.setGivenName(championData[1].split(" ")[0]);
            driver.setNationality(championData[0]);
            drivers.add(driver);

        }
        return drivers;
    }
}
