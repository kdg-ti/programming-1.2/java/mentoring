package be.kdg.mentoring.week04.f1.repository.ergas;

import be.kdg.mentoring.week04.f1.model.Constructor;
import com.fasterxml.jackson.core.type.TypeReference;

public class ConstructorRepository extends ErgasF1Repository<Constructor> {

    public ConstructorRepository() {
        this(constructDefaultUrl("constructors"));
    }

    public ConstructorRepository(String apiUrl) {
        super(apiUrl, new TypeReference<>() {
        });
    }
}
