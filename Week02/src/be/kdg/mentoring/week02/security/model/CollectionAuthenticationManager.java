package be.kdg.mentoring.week02.security.model;

import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.TreeSet;

public class CollectionAuthenticationManager implements AuthenticationManger {
//    Map<String, String> usersMap = new HashMap<>();
    Collection<Account> users = new TreeSet<>();

    @Override
    public boolean authenticate(String username, String password) {
        // get the password associate with the supplied username
        String storedPwd = null;

        Iterator<Account> accountIterator = users.iterator();
        while (accountIterator.hasNext()) {
            System.out.printf("User [%s] is found", username);
            Account account = accountIterator.next();
            if(username.equals(account.getUsername()) && password.equals(account.getPassword())) {
                System.out.printf("User [%s] successfully logged in", username);
                return true;
            }
        }

        System.err.printf("User [%s] could not logged in\n", username);
        return false;
    }

    @Override
    public void register(String username, String password) {
        System.out.printf("Registering user %s\n", username);
        Account account = new Account(username, password, null);
        users.add(account);
    }

    @Override
    public Collection<Account> getUsers() {
        return Collections.unmodifiableCollection(users);
    }
}
