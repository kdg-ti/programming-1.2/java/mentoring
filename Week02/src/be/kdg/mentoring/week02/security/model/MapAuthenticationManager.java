package be.kdg.mentoring.week02.security.model;

import java.util.Collection;
import java.util.Map;
import java.util.TreeMap;

public class MapAuthenticationManager implements AuthenticationManger {
//    Map<String, String> usersMap = new HashMap<>();
    Map<String, String> usersMap = new TreeMap<>();
    @Override
    public boolean authenticate(String username, String password) {
        // get the password associate with the supplied username
        String storedPwd = usersMap.get(username);

        if(storedPwd == null) {
            // the username wasnt stored!
            System.err.printf("User %s does not exist\n", username);
            return false;
        }
        // if the password dont match.
        if(!storedPwd.equals(password)) {
            System.err.printf("Invalid password for user %s\n", username);
            return false;
        }

        System.out.printf("User %s is logged in\n", username);
        return true;
    }

    @Override
    public void register(String username, String password) {
        System.out.printf("Registering user %s\n", username);
        usersMap.put(username, password);
    }

    @Override
    public Collection<Account> getUsers() {
        return null;
    }
}
