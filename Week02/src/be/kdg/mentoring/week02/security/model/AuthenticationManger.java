package be.kdg.mentoring.week02.security.model;

import java.util.Collection;

public interface AuthenticationManger {

    boolean authenticate(String username, String password);

    void register(String username, String password);

    Collection<Account> getUsers();
}
