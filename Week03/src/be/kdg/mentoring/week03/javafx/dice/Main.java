package be.kdg.mentoring.week03.javafx.dice;

import be.kdg.mentoring.week03.javafx.dice.view.DicePresenter;
import be.kdg.mentoring.week03.javafx.dice.model.Dice;
import be.kdg.mentoring.week03.javafx.dice.view.DiceView;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

//imports...
public class Main extends Application {

    public static void main(String[] args) {
        Application.launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        int numberOfDice = 4;
        Dice model = new Dice(numberOfDice);
        DiceView view = new DiceView(numberOfDice);

        primaryStage.setTitle("Dice");
//        primaryStage.setWidth(400);
//        primaryStage.setHeight(250);
//        GridPane grid = new GridPane();
//        grid.add(new Label("Left"), 0, 0);
//        grid.add(new Label("Center"), 1, 0);
//        grid.add(new Label("Right"), 2, 0);
//        grid.add(view, 1, 1);
//        grid.setBorder(new Border(new BorderStroke(Color.BLACK, BorderStrokeStyle.SOLID, null, BorderStroke.THICK)));
//        primaryStage.setScene(new Scene(grid));
        primaryStage.setScene(new Scene(view));
        new DicePresenter(model, view);
        primaryStage.show();
    }
}

