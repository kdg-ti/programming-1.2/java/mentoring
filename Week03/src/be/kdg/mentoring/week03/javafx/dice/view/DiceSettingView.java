package be.kdg.mentoring.week03.javafx.dice.view;

import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;

public class DiceSettingView extends GridPane {

    // private Node attributes (controls)
    private ChoiceBox numberOfDice;
    private Label infoLabel;

    private Button okButton;

    private Button cancelButton;

    public DiceSettingView() {
        initialiseNodes();
        layoutNodes();
    }

    private void initialiseNodes() {
        // create and configure controls
        okButton = new Button("Ok");
        cancelButton = new Button("Cancel");
        infoLabel = new Label("Number of dice");
        numberOfDice = new ChoiceBox();
        for (int i = 0; i < 40; i++) {
            numberOfDice.getItems().add(i);
        }
    }

    private void layoutNodes() {
        infoLabel.setText("Number of dice");
        add(infoLabel, 0, 0);
        GridPane.setHalignment(infoLabel, HPos.LEFT);
        add(numberOfDice, 1, 0, 2, 1);
        GridPane.setHalignment(numberOfDice, HPos.RIGHT);

        add(cancelButton, 1, 1);
        GridPane.setHalignment(okButton, HPos.CENTER);

        add(okButton, 1, 1);
        GridPane.setHalignment(okButton, HPos.CENTER);

//        setPrefWidth(80);
        setVgap(10);
        setHgap(20);
        setPadding(new Insets(10));

    }
    // package-private Getters
    // for controls used by Presenter

    Button getOkButton() {
        return okButton;
    }

    Button getCancelButton() {
        return cancelButton;
    }
}
