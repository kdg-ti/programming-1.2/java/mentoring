package be.kdg.mentoring.week03.javafx.dice.model;

import java.util.LinkedList;
import java.util.List;

public class Dice {
    List<Die> dice = new LinkedList<>();

    public Dice(int numberOfDice) {
        for (int i = 0; i < numberOfDice; i++) {
            dice.add(new Die());
        }
    }
    public int getNumberOfDice() {
        return dice.size();
    }
    public void setNumberOfDice(int numberOfDice) {
        if(numberOfDice == dice.size()) {
            return;
        }
        while (numberOfDice != dice.size()) {
            if(dice.size() > numberOfDice)
                dice.remove(0);
            else
                dice.add(new Die());
        }
    }
    public void roll() {
        for (Die die : dice) {
            die.roll();
        }
    }

    /**
     * Returns the face value of the dice at the specified dieIndex
     * @param dieIndex index 0 for the first one.
     * @return int representing the face value of the specified Die.
     */
    public int getFaceValue(int dieIndex) {
        return dice.get(dieIndex).getFaceValue();
    }

    public int getTotalFaceValues() {
        return dice.stream().mapToInt(Die::getFaceValue).sum();
    }

    @Override
    public String toString() {
        return "Dice{" +
                "dice=" + dice +
                '}';
    }
}
