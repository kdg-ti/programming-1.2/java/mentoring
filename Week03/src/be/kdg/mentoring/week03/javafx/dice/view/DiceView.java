package be.kdg.mentoring.week03.javafx.dice.view;

import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class DiceView extends GridPane {
    // private Node attributes (controls)
    private final List<ImageView> diceView;
    private final int numberOfDice;
    private Label infoLabel;

    private Button rollButton;

    public DiceView(int numberOfDice) {
        this.numberOfDice = numberOfDice;
        diceView = Collections.synchronizedList(new LinkedList<>());
        initialiseNodes();
        layoutNodes();
    }

    private void initialiseNodes() {
        // create and configure controls
        rollButton = new Button("Roll");
        infoLabel = new Label("Click to Start rolling");
        setDice(numberOfDice);
    }

    private void setDice(int numberOfDice) {
        while ( diceView.size() != numberOfDice) {
            if(diceView.size() > numberOfDice) {
                diceView.remove(diceView.size() - 1);
            } else if(diceView.size() < numberOfDice){
                diceView.add(new ImageView());
            }
        }
    }

    void updateNumberOfDice(int numberOfDice) {
        setDice(numberOfDice);
        getChildren().clear();
        layoutNodes();
    }


    private void layoutNodes() {
        int colspan = 4;
        int col = 0, row = 0;
        int maxNrOfCol = 3;
        for (ImageView dieImageView : diceView) {
            add(dieImageView, col, row);
            dieImageView.setFitHeight(50);
            dieImageView.setFitWidth(50);
            if(col++ == maxNrOfCol) {
                row++;
                col = 0;
            }
        }
        if(col > 0) {
            row++;
        }
        add(rollButton, 0, row++, colspan, 1);
        GridPane.setHalignment(rollButton, HPos.CENTER);

        add(infoLabel, 0, row, colspan, 1);
        GridPane.setHalignment(infoLabel, HPos.CENTER);

        infoLabel.setText(String.format("Click to Start rolling (%d rows)", row));

        setPrefWidth(80);
        setVgap(10);
        setHgap(20);
        setPadding(new Insets(10));

        setGridLinesVisible(true);

    }
    // package-private Getters
    // for controls used by Presenter

    Button getRollButton() {
        return rollButton;
    }

    Label getInfoLabel() {
        return infoLabel;
    }

    List<ImageView> getDiceView() {
        return diceView;
    }
}
