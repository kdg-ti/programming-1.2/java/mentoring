package be.kdg.mentoring.week03.javafx.dice.model;

import java.util.Random;

public class Die {
    private int faceValue;
    private static final Random RANDOM = new Random();
    private static final int DICE_MAX_VALUE = 6;

    public void roll() {
        faceValue = RANDOM.nextInt(DICE_MAX_VALUE) + 1;
    }

    public int getFaceValue() {
        return faceValue;
    }

    @Override
    public String toString() {
        return "Die{" +
                "faceValue=" + faceValue +
                '}';
    }
}
