package be.kdg.mentoring.week03.javafx.sandbox;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.scene.transform.Translate;
import javafx.stage.Stage;

public class TranslateTransformationExample extends Application {

    public static void main(String[] args) {
        Application.launch(args);
    }

    public void start(Stage primaryStage) {

        ImageView imageViewOriginal  = createImageView();
        ImageView imageViewTranslated = createImageView();

        Translate translateTransform = new Translate();
        translateTransform.setX(200);
        translateTransform.setY(200);

        imageViewTranslated.getTransforms().add(translateTransform);

        Pane pane = new Pane();
        pane.getChildren().add(imageViewTranslated);
        pane.getChildren().add(imageViewOriginal);

        Scene scene = new Scene(pane, 1024, 800, true);
        primaryStage.setScene(scene);
        primaryStage.setTitle("2D Example");

        primaryStage.show();
    }

    private ImageView createImageView() {
//        Image image = new Image("/images/abstract/abstract-ai-001.jpg");
        Image image = new Image("/images/abstract/abstract-5719221_640.jpg");
        ImageView imageView = new ImageView(image);
        return imageView;
    }

}
