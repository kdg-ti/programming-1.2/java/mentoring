package be.kdg.mentoring.week03.javafx.screenreader.model;

import com.sun.speech.freetts.Voice;
import com.sun.speech.freetts.VoiceManager;

public class ScreenReader {
    private String voiceName;
    private String text = "No text";

    public ScreenReader() {
        // set a library system property specific
        System.setProperty("freetts.voices",
                "com.sun.speech.freetts.en.us.cmu_us_kal.KevinVoiceDirectory");
        this.voiceName = "kevin16";
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }

    public void readAloud() {
        Voice voice = VoiceManager.getInstance().getVoice(voiceName);
        voice.allocate();
        voice.speak(text);
        voice.deallocate();
    }

}
