package be.kdg.mentoring.week03.javafx.screenreader;

import be.kdg.mentoring.week03.javafx.screenreader.model.ScreenReader;
import be.kdg.mentoring.week03.javafx.screenreader.view.ScreenReaderPresenter;
import be.kdg.mentoring.week03.javafx.screenreader.view.ScreenReaderView;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {

    public static void main(String[] args) {
        Application.launch(args);
    }
    @Override // Override the start method in the Application class
    public void start(Stage primaryStage) {

        ScreenReader model = new ScreenReader();
        model.setText("What can I say?");
        ScreenReaderView view = new ScreenReaderView();

        primaryStage.setTitle("Screen Reader");
        primaryStage.setWidth(300);
        primaryStage.setHeight(450);
        primaryStage.setScene(new Scene(view));
        ScreenReaderPresenter presenter = new ScreenReaderPresenter(model, view);
        primaryStage.show();
    }
}
