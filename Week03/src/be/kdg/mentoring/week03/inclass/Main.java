package be.kdg.mentoring.week03.inclass;

import java.util.List;

public class Main {
    public static void main(String[] args) {
        PersonRepository database = new PersonRepository();
        database.add(new Person("Angele",27,'F' ));
        database.add(new Person("Romeo Elvis",30,'M' ));
        database.add(new Person("Marka",60,'M' ));
        database.add(new Person("Lisette",1,'F' ));

        System.out.println("Printing all adults: " +
                database.getBy(new AdultTester()));

        System.out.println("Printing all ladies: " +
                database.getBy(p -> {return p.getGender() == 'F';}));
        System.out.println("Printing persons with multi word names: " +
                database.getBy(p -> p.getName().contains(" ")));

        List<Person> withE = database.getBy(p -> p.getName().contains("a") || p.getName().contains("A"));
        System.out.println("Printing persons with a in names: " + withE);
        withE.removeIf(p -> p.getGender() == 'M');
        System.out.println("Printing persons with a in names and gender M: " + withE);
        //…
    }
}
