package be.kdg.mentoring.week03.functional;

// Java code to illustrate lambda expression
// with multi parameters
public class MultiParamsLambdaTest {

    // functional interface MultiParamsLambda
    // with 2 parameter of Integer type
    interface MultiParamsLambda {
        // The void type and the Integer type
        // is automatically inferred from here
        // and assigned to the lambda expression
        void print(Integer p, Integer p2);
    }

    // takes parameter of MultiParamsLambda type followed
    // by 2 integer parameters p1 and p2
    static void fun(MultiParamsLambda t, Integer p1, Integer p2){
        // calls the print function
        t.print(p1, p2);
    }
    public static void main(String[] args)
    {
        // lambda expression is passed
        // with a two parameter
        // lambda expression is mapped to the
        // double argument abstract function in the
        // functional interface MultiParamsLambda
        fun((p1, p2) -> System.out.println(p1 + " " + p2), 10, 20);
    }
}
