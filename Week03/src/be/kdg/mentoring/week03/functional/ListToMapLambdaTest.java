package be.kdg.mentoring.week03.functional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ListToMapLambdaTest {
    public static void main(String[] args) {
        List<KeyValueObject> keyValueObjectList = List.of(
            new KeyValueObject(1, "Shahada (الشهادتان)"),
            new KeyValueObject(2, "Prayer (إقام الصلاة)") ,
                new KeyValueObject(3, "Zakat (إيتاء الزكاة)"),
                new KeyValueObject(4, "Fasting Ramadan (صوم شهر رمضان)"),
                new KeyValueObject(5, "Hadj to Mekka (حج البيت)")
        );

        Map<Integer, String> map = new HashMap<>();
        keyValueObjectList.forEach(n -> map.put(n.getKey(), n.getValue()));
        System.out.println(map);
    }
}
class KeyValueObject {
    private Integer key;
    private String value;

    public KeyValueObject(Integer key, String value) {
        this.key = key;
        this.value = value;
    }

    public Integer getKey() {
        return key;
    }

    public String getValue() {
        return value;
    }
}
