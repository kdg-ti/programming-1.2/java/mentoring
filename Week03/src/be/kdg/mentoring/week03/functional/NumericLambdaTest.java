package be.kdg.mentoring.week03.functional;

public class NumericLambdaTest {
    interface NumericLambda {
        boolean test(int a, int b);
    }

    static NumericLambda isFactor = (a, b) -> (a%b) == 0;

    public static void main(String[] args) {
        NumericLambda isFactor = (a, b) -> (a%b) == 0;

        if(isFactor.test(10, 2))
            System.out.println("2 is not factor of 10");
        if(!isFactor.test(10, 3))
            System.out.println("3 is not factor of 10");

        isFactor(10, 2);
        isFactor(10, 3);
    }

    private static void isFactor(int a, int b) {
        if(isFactor.test(a, b))
            System.out.printf("%d is factor of %d\n", a, b);
        else
            System.out.printf("%d is not factor of %d\n", a, b);
    }
}
