package be.kdg.mentoring.week03.sorting;

import java.util.List;
//our own quick sort() algorithm
public class QuickSort {
    // Main sorting method to be called by the user
    public static void quickSort(List<Integer> list) {
        if (list == null || list.isEmpty()) {
            return;
        }
        quickSort(list, 0, list.size() - 1);
    }

    // Recursive helper method for Quick Sort
    private static void quickSort(List<Integer> list, int low, int high) {
        if (low < high) {
            // Find the partition index
            int partitionIndex = partition(list, low, high);

            // Recursively sort elements before and after the partition
            quickSort(list, low, partitionIndex - 1);
            quickSort(list, partitionIndex + 1, high);
        }
    }

    // Partitioning method to find the correct position of the pivot
    private static int partition(List<Integer> list, int low, int high) {
        // Choose the pivot element (in this case, the last element)
        int pivot = list.get(high);
        int i = low - 1;

        // Iterate through the list and move elements smaller than the pivot to the left
        for (int j = low; j < high; j++) {
            if (list.get(j) < pivot) {
                i++;
                swap(list, i, j);
            }
        }

        // Swap the pivot element to its correct position
        swap(list, i + 1, high);
        return i + 1;
    }

    // Helper method to swap two elements in the list
    private static void swap(List<Integer> list, int i, int j) {
        int temp = list.get(i);
        list.set(i, list.get(j));
        list.set(j, temp);
    }

    public static void main(String[] args) {
        List<Integer> numbers = List.of(7, 2, 1, 6, 8, 5, 3, 4);
        System.out.println("Unsorted List: " + numbers);
        quickSort(numbers);
        System.out.println("Sorted List: " + numbers);
    }
}