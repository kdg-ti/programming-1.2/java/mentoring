package be.kdg.mentoring.week03.sorting;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class SortList {
    public static void main(String[] args) {
        List<Student> students = new ArrayList<>();
        students.add(new Student("Johnny", 11));
        students.add(new Student("Ivy", 13));
        students.add(new Student("Rick", 12));

        // Sort ArrayList of students by age in ascending order using a custom comparator
        Collections.sort(students, new Comparator<Student>() {
            @Override
            public int compare(Student s1, Student s2) {
                return Integer.compare(s1.getAge(), s2.getAge());
            }
        });

        // Display the sorted list of students
        for (Student student : students) {
            System.out.println(student.getName() + " - " + student.getAge() + " years old");
        }
    }
}