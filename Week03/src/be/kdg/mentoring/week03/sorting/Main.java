package be.kdg.mentoring.week03.sorting;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        // Create a list of string instruments in a quartet
        List<String> quartet = new ArrayList<>();
        quartet.add("Violin");
        quartet.add("Viola");
        quartet.add("Cello");
        quartet.add("Double Bass");

        // Display the original list
        System.out.println("Original quartet: " + quartet);

        // Sort the quartet using lambda expression (in ascending order)
        quartet.sort((instrument1, instrument2) -> instrument1.compareTo(instrument2));

        // Display the sorted quartet
        System.out.println("Sorted quartet (ascending): " + quartet);
    }
}