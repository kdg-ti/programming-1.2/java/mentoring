package be.kdg.mentoring.week03.sorting;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class SimpleListSorting {
    public static void main(String[] args) {
        // Create a list of integers
        List<Integer> numbers = new ArrayList<>();
        numbers.add(5);
        numbers.add(1);
        numbers.add(3);
        numbers.add(2);

        // Display the original list
        System.out.println("Original list: " + numbers);

        // Sort ArrayList in ascending order
        Collections.sort(numbers);

        // Display the sorted list
        System.out.println("Sorted list (ascending): " + numbers);
    }
}
