package be.kdg.mentoring.week03.streams;

// Java Program to demonstrate
// File Read Operation

import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;

public class IOWriteStreamExample {

    public static void main(String[] args) {
        String[] words
                = {"Hello", "there", "how", "are", "you", "doing", "over", "there", "?"};
        String fileName = "newFile.txt";

        // Step 1: Create a Stream of lines from the
        // file
        try (PrintWriter pw = new PrintWriter(
                Files.newBufferedWriter(Paths.get(fileName)))) {
            Stream.of(words).forEach(pw::println);
            System.out.println("Write operation is successfully executed!");
        } catch(IOException e) {
            e.printStackTrace();
        }
    }
}

