package be.kdg.mentoring.week03.predicates;

import java.util.function.Predicate;

public class PredicateAndDemo {
    public static void main(String[] args) {
        Predicate<Integer> adultYet = i -> i >= 18;
        Predicate<Integer> adultStill = i -> i < 65;
        System.out.println(adultYet.and(adultStill).test(5));
        System.out.println(adultYet.and(adultStill).test(35));
        System.out.println(adultYet.and(adultStill).test(85));
    }
}
