package be.kdg.mentoring.week03.predicates;

import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.function.Predicate;

public class PredicateNegateDemo {
    public static void main(String[] args) {

        Predicate<Tomato> greenTomato = t -> t.color.equalsIgnoreCase("green");
        Collection<Tomato> tomotoBasket = List.of(
                new Tomato("white", 1),
                new Tomato("red", 2),
                new Tomato("orange", 2),
                new Tomato("red", 1),
                new Tomato("green", 2)
        );
        System.out.println("Full basked" + tomotoBasket);

        List<Tomato> nonRedTomatos = negateByColor(tomotoBasket, "red");
        System.out.println("Red excluded" + nonRedTomatos);

        List<Tomato> nonGreenTomatos = negateByColor(tomotoBasket, "green");
        System.out.println("Green excluded" + nonGreenTomatos);

        System.out.println("Full basked" + tomotoBasket);
    }

    private static List<Tomato> negateByColor(Collection<Tomato> tomotoBasket, String color) {
        Predicate<Tomato> readTomato = t -> t.color.equalsIgnoreCase(color);
        List<Tomato> filtered = tomotoBasket.stream().filter(readTomato.negate()).toList();
        return filtered;
    }
}

class Tomato {
    final String color;
    final int size;

    Tomato(String color, int size) {
        this.color = color;
        this.size = size;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Tomato tomato = (Tomato) o;
        return size == tomato.size && Objects.equals(color, tomato.color);
    }

    @Override
    public int hashCode() {
        return Objects.hash(color, size);
    }

    @Override
    public String toString() {
        return "Tomato{" +
                "color='" + color + '\'' +
                ", size=" + size +
                '}';
    }
}
