package be.kdg.mentoring.week03.predicates;

import java.util.Arrays;
import java.util.function.IntPredicate;

/**
 * source: https://codegym.cc/groups/posts/java-predicate
 * Java IntPredicate
 * Java IntPredicate is a functional interface, so you can use it as the assignment target for a lambda expression or method reference. IntPredicate operates on an integer and returns a predicate value based on a condition.
 *
 * Such as Predicate Interface, IntPredicate also has test(), and(), negate(), or() methods.
 *
 * Here’s an example of IntPredicate. It also filters all the adults (18 or more) from the array.
 */
public class IntPredicateDemo {
    public static void main(String[] args) {

        int[] ages = { 18, 28, 18, 46, 90, 45, 2, 3, 1, 5, 7, 21, 12 };
        IntPredicate p = n -> n >= 18;
        // System.out::println means that System.out.println method
        // will be called for each element in the filtered array (stream)
        Arrays.stream(ages).filter(p).forEach(System.out::println);
    }
}
