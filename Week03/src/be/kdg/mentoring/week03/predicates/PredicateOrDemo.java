package be.kdg.mentoring.week03.predicates;

import java.util.function.Predicate;

public class PredicateOrDemo {
    public static void main(String[] args) {
        Predicate<String> containsA = t -> t.contains("crayon");
        Predicate<String> containsB = t -> t.contains("my");
        System.out.println(containsA.or(containsB).test("here is my crayon"));
        System.out.println(containsA.or(containsB).test("here is my pencil"));
        System.out.println(containsA.or(containsB).test("here is John's crayon"));
        System.out.println(containsA.or(containsB).test("here is John's pencil"));
    }
}
