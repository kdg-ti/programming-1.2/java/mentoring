package be.kdg.mentoring.week03.predicates;

import java.util.Collection;
import java.util.List;
import java.util.function.Predicate;

public class PredicateEqualsDemo {
    public static void main(String[] args) {
        Collection<Tomato> tomotoBasket = List.of(
                new Tomato("white", 1),
                new Tomato("red", 2),
                new Tomato("orange", 2),
                new Tomato("red", 1),
                new Tomato("green", 2),
                new Tomato("white", 1),
                new Tomato("red", 2),
                new Tomato("orange", 2),
                new Tomato("red", 1),
                new Tomato("green", 2),
                new Tomato("white", 1),
                new Tomato("red", 2),
                new Tomato("orange", 2),
                new Tomato("red", 1),
                new Tomato("green", 2)
        );
        System.out.println("Full basked" + tomotoBasket);

        List<Tomato> nonRedTomatos = filterEqual(tomotoBasket, new Tomato("red", 2));
        System.out.println("Red with size 2 only: " + nonRedTomatos);

        List<Tomato> nonGreenTomatos = filterEqual(tomotoBasket, new Tomato("white", 1));
        System.out.println("White with size 1" + nonGreenTomatos);

        System.out.println("Full basked" + tomotoBasket);
    }

    private static List<Tomato> filterEqual(Collection<Tomato> tomotoBasket, Tomato tomato) {
        Predicate<Tomato> readTomato = t -> t.equals(tomato);
        List<Tomato> filtered = tomotoBasket.stream().filter(readTomato).toList();
        return filtered;
    }
}