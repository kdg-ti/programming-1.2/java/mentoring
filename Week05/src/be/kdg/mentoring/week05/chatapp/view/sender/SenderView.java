package be.kdg.mentoring.week05.chatapp.view.sender;

import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.layout.VBox;

public class SenderView extends VBox {
    private Button button;
    private TextArea messagesArea;

    public SenderView() {
        initializeNodes();
        layoutNodes();
    }

    private void initializeNodes() {
        button = new Button("Send");
        messagesArea = new TextArea();
    }

    private void layoutNodes() {
        getChildren().addAll(messagesArea, button);
    }

    Button getButton() {
        return button;
    }

    TextArea getMessagesArea() {
        return messagesArea;
    }
}
