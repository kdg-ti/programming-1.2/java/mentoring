package be.kdg.mentoring.week05.chatapp.view;

import be.kdg.mentoring.week05.chatapp.model.ChatModel;
import be.kdg.mentoring.week05.chatapp.view.receiver.ReceiverView;
import be.kdg.mentoring.week05.chatapp.view.sender.SenderPresenter;
import be.kdg.mentoring.week05.chatapp.view.sender.SenderView;
import be.kdg.mentoring.week05.chatapp.view.receiver.ReceiverPresenter;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class MainPresenter {
    ChatModel model;
    MainView view;

    public MainPresenter(ChatModel model, MainView view) {
        this.model = model;
        this.view = view;
        addEventHandlers();
    }

    private void addEventHandlers() {
        view.getSenderButton().setOnAction( event -> {
            SenderView senderView = new SenderView();
            SenderPresenter senderPresenter = new SenderPresenter(model, senderView);
            startStageForView(senderView);
        });

        view.getReceiverBUtton().setOnAction(event -> {
            ReceiverView receiverView = new ReceiverView();
            ReceiverPresenter receiverPresenter = new ReceiverPresenter(model, receiverView);
            startStageForView(receiverView);
        });
    }

    private static void startStageForView(Parent senderView) {
        // create a new Sender view
        // create a new Sender Presenter
        // Then stage, scene and show the view.
        Stage stage = new Stage();
        stage.setScene(new Scene(senderView));
        stage.show();
    }


}
