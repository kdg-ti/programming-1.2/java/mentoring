package be.kdg.mentoring.week05.chatapp.view.receiver;

import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.layout.VBox;

public class ReceiverView extends VBox {
    private Button button;
    private TextArea messagesArea;

    public ReceiverView() {
        initializeNodes();
        layoutNodes();
    }

    private void initializeNodes() {
        button = new Button("Get Messages");
        messagesArea = new TextArea();
    }

    private void layoutNodes() {
        getChildren().addAll(messagesArea, button);
    }

    Button getButton() {
        return button;
    }

    TextArea getMessagesArea() {
        return messagesArea;
    }
}
