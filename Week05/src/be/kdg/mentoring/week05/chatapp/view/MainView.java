package be.kdg.mentoring.week05.chatapp.view;

import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;

public class MainView extends GridPane {
    private Button senderButton;
    private Button receiverBUtton;

    private Label welcomeLabel;

    public MainView() {
        initializeNodes();
        layoutNodes();
    }

    private void initializeNodes() {
        senderButton = new Button("Sender");
        receiverBUtton = new Button("Receiver");
        welcomeLabel = new Label("Welcome to ACS Chat Program!");
    }

    private void layoutNodes() {
//        GridPane gridPane = new GridPane();
        add(welcomeLabel, 0, 0, 2, 1);
        add(senderButton, 0, 1);
        add(receiverBUtton, 1, 1);
//        add(gridPane, 0, 0);
    }

    Button getSenderButton() {
        return senderButton;
    }

    Button getReceiverBUtton() {
        return receiverBUtton;
    }

    Label getWelcomeLabel() {
        return welcomeLabel;
    }
}
