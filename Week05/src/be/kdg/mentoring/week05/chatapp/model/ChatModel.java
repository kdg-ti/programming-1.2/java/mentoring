package be.kdg.mentoring.week05.chatapp.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ChatModel {
    List<String> messages = new ArrayList<>();

    public void addMessage(String msg) {
        messages.add(msg);
    }

    public List<String> getMessages() {
        return Collections.unmodifiableList(messages);
    }

}
